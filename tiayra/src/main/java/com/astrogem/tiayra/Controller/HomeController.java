package com.astrogem.tiayra.Controller;

import com.astrogem.tiayra.Model.Admin;
import com.astrogem.tiayra.Model.Appointment;
import com.astrogem.tiayra.Model.Login;
import com.astrogem.tiayra.Model.Token;
import com.astrogem.tiayra.Services.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    Services services;

    @GetMapping("/get")
    String get()
    {
        return "here";
    }

    @CrossOrigin
    @PostMapping("/take")
    String takeAppointmrnt(@RequestBody Appointment request)
    {
        System.out.println(request.getBranch());
        try{
            return services.appoinment(request);
        }catch (Exception e)
        {
            return "error";
        }
    }

    @CrossOrigin
    @PostMapping("/login")
    Token login(@RequestBody Login request)
    {
        Token token = new Token();
        try{
            return services.login(request);
        }catch (Exception e)
        {
            token.setToken("error");
            return token;
        }
    }


    @CrossOrigin
    @PostMapping("/check")
    Token check(@RequestBody Token request)
    {
        Token token = new Token();
        try{
            return services.check(request.getToken());
        }catch (Exception e)
        {
            token.setToken("error");
            return token;
        }
    }

    @CrossOrigin
    @PostMapping("/get/{status}")
    List<Appointment> login(@RequestBody Token request, @PathVariable String status)
    {
        Token token = new Token();
        try{
            return services.getAppointment(request.getToken(), status);
        }catch (Exception e)
        {
            token.setToken("error");
            return null;
        }
    }

    @CrossOrigin
    @PostMapping("/update/{id}/{status}")
    Token update(@RequestBody Token request, @PathVariable String id, @PathVariable String status)
    {
        Token token = new Token();
        try{
            token = services.update(request.getToken(), status, id);
            return token;
        }catch (Exception e)
        {
            return token;
        }
    }

    @CrossOrigin
    @PostMapping("/updateUser/{id}/{branch}")
    Token updateUser(@RequestBody Token request, @PathVariable String id, @PathVariable String branch)
    {
        Token token = new Token();
        try{
            token = services.updateUser(request.getToken(), branch, id);
            return token;
        }catch (Exception e)
        {
            return token;
        }
    }

    @CrossOrigin
    @PostMapping("/saveUser/{name}/{password}/{phone}/{branch}")
    Token saveUser(@RequestBody Token request, @PathVariable String name, @PathVariable String password, @PathVariable String phone, @PathVariable String branch)
    {
        Token token = new Token();
        try{
            token = services.saveUser(request.getToken(), name, password, phone, branch);
            return token;
        }catch (Exception e)
        {
            return token;
        }
    }

    @CrossOrigin
    @PostMapping("/deleteUser/{id}")
    Token delleteUser(@RequestBody Token request, @PathVariable String id)
    {
        Token token = new Token();
        try{
            return services.deleteUser(request.getToken(), id);
        }catch (Exception e)
        {
            token.setToken("not deleted");
            return token;
        }
    }

    @CrossOrigin
    @PostMapping("/getUsers")
    List<Admin> update(@RequestBody Token request)
    {
        Token token = new Token();
        try{
            return services.getUsers(request.getToken());
        }catch (Exception e)
        {
            return null;
        }
    }

}
