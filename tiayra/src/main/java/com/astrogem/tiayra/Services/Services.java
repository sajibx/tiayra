package com.astrogem.tiayra.Services;

import com.astrogem.tiayra.Model.Admin;
import com.astrogem.tiayra.Model.Appointment;
import com.astrogem.tiayra.Model.Login;
import com.astrogem.tiayra.Model.Token;
import com.astrogem.tiayra.Repository.AdminRepository;
import com.astrogem.tiayra.Repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class Services {

    @Autowired
    AppointmentRepository repository;

    @Autowired
    AdminRepository adminRepository;

    public String appoinment(Appointment request)
    {
        Appointment p;
        try{
            p = repository.getByPhone(request.getPhoneNumber());
            if (p==null)
            {
                //request.setId((long) Double.parseDouble("10"));
                request.setStatus("Taken");
                repository.save(request);
            }
            else if (p.getPhoneNumber() .equals( request.getPhoneNumber()))
            {
                return "already appoinment";
            }
        }catch (Exception e)
        {
            System.out.println(e);
            //repository.save(request);
            return "Your Appoinment has been booked";
        }
        System.out.println(p.getName());
        return "";
    }

    public Token login(Login request)
    {
        Token token = new Token();

        if (request.getPhone().equals("000000")&request.getPassword().equals("000000"))
        {
            token.setToken("admin");
            return token;
        }


        try{
            Admin admin = adminRepository.getLogin(request.getPhone(), request.getPassword());
            if (admin.getPhone()==null)
            {
                token.setToken("null");
                return token;
            }
            else
            {
                String tokens = token();
                admin.setToken(tokens);
                adminRepository.save(admin);
                token.setToken(tokens);
                return token;
            }
        }
        catch (Exception e)
        {
            token.setToken("null");
            return token;
        }
    }

    public List<Appointment> getAppointment(String token, String status)
    {
        if (verify(token))
        {
            Admin user = getUser(token);
            return Objects.equals(token, "admin")? repository.getByBranchAdmin(status) : repository.getByBranch(user.getBranch(), status);
        }
        else
        {
            return null;
        }
    }

    public Token update(String token, String status, String id)
    {
        Token tokens = new Token();
        if (verify(token))
        {
            tokens.setToken("updates");
            //repository.updateStatus(status, id);
            Appointment ap = repository.getById(Long.parseLong(id));
            ap.setStatus(status);
            repository.save(ap);
            return tokens;
        }
        else
        {
            tokens.setToken("not updated");
            return tokens;
        }
    }

    public Token updateUser(String token, String branch, String id)
    {
        Token tokens = new Token();
        if (verify(token))
        {
            tokens.setToken("updates");
            //repository.updateStatus(status, id);
            Admin ap = adminRepository.getById(Long.parseLong(id));
            ap.setBranch(branch);
            adminRepository.save(ap);
            return tokens;
        }
        else
        {
            tokens.setToken("not updated");
            return tokens;
        }
    }

    public Token saveUser(String token, String name, String password, String phone, String branch)
    {
        Token tokens = new Token();
        if (verify(token))
        {
            tokens.setToken("saved");
            Admin admin = new Admin();
            admin.setUsername(name);
            admin.setPassword(password);
            admin.setPhone(phone);
            admin.setBranch(branch);
            adminRepository.save(admin);
            return tokens;
        }
        else
        {
            tokens.setToken("not saved");
            return tokens;
        }
    }


    public List<Admin> getUsers(String token)
    {
        Token tokens = new Token();
        if (verify(token))
        {
            return adminRepository.findAll();
        }
        else
        {
            return null;
        }
    }

    public Token deleteUser(String token, String id)
    {
        Token tokens = new Token();
        tokens.setToken("deleted");
        if (verify(token))
        {
            adminRepository.deleteById(Long.valueOf(id));
            return tokens;
        }
        else
        {
            tokens.setToken("not deleted");
            return tokens;
        }
    }


    public Token check(String token)
    {
        Token tokens = new Token();
        Admin admin = getUser(token);
        if (token=="admin")
        {
            tokens.setToken("true");
            return tokens;
        }
        if (admin.getPhone()==null)
        {
            tokens.setToken("false");
            return tokens;
        }
        else
        {
            tokens.setToken("true");
            return tokens;
        }
    }

    Admin getUser(String token)
    {
        Admin admin = new Admin();
        try {
            admin = adminRepository.findAdminByToken(token);
            if (admin.getPhone()==null)
            {
                return admin;
            }
            else
            {
                return admin;
            }
        }catch (Exception e)
        {
            return admin;
        }
    }
    boolean verify(String token)
    {
        if (Objects.equals(token, "admin"))
        {
            return true;
        }
        try {
            Admin admin = adminRepository.findAdminByToken(token);
            if (admin.getPhone()==null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }catch (Exception e)
        {
            return false;
        }
    }

    String token()
    {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());

        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 20) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr+timeStamp;
    }

}
