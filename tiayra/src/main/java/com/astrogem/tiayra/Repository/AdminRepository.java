package com.astrogem.tiayra.Repository;

import com.astrogem.tiayra.Model.Admin;
import com.astrogem.tiayra.Model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdminRepository extends JpaRepository<Admin, Long> {

    @Query("SELECT f FROM Admin f WHERE f.phone = :phone and f.password = :password")
    Admin getLogin(@Param("phone") String phone, @Param("password") String password);

    Admin findAdminByToken(String token);

}
