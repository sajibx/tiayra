package com.astrogem.tiayra.Repository;

import com.astrogem.tiayra.Model.Admin;
import com.astrogem.tiayra.Model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    @Query("select  p from Appointment  p where p.phoneNumber  >?1")
    Appointment geByPhoneNumber(String phoneNumber);

    @Query("SELECT f FROM Appointment f WHERE f.phoneNumber = :phone")
    Appointment getByPhone(@Param("phone") String phone);

    @Query("SELECT f FROM Appointment f WHERE f.branch LIKE %:branch% and f.status =:status order by f.id desc")
    List<Appointment> getByBranch(@Param("branch") String branch, @Param("status") String status);

    @Query("SELECT f FROM Appointment f WHERE f.status =:status order by f.id desc")
    List<Appointment> getByBranchAdmin( @Param("status") String status);

    @Query("UPDATE Appointment f set f.status =:status where f.id = :id")
    void updateStatus( @Param("status") String status, @Param("id") String id);


}
