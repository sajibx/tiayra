package com.astrogem.tiayra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiayraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiayraApplication.class, args);
	}

}
