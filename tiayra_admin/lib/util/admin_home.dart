import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/NetworkHelper.dart';

class admin_home extends StatefulWidget {
  const admin_home({Key? key}) : super(key: key);

  @override
  State<admin_home> createState() => _admin_homeState();
}

class _admin_homeState extends State<admin_home> {


  var items;
  int data_s = 0;
  int id = 0;
  String status = 'Taken';
  String state = "";
  var item = [
    'Taken',
    'Pending',
    'Closed'
  ];
  void testData() async
  {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    //log("here 1");
    var data = await networkHelper.getAppointment(prefs.getString('token')!, status);
    var data_ = json.decode(data.body);

    //log("sajib 2 - ");

    //Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';
    var map = jsonDecode(data.body); // import 'dart:convert';
    setState(() {
      data_s = data_.length;
      items = map;
    });

    // log("sajib 1 - "+map[0]['name']);
    // //final datas = data_ as Map;
    // log("sajib 1 - "+data_.length.toString());
    // log("sajib 1 - "+data_s.toString()+"----"+map[0]['name']);
  }

  void logout() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Navigator.pushNamedAndRemoveUntil(context, "/", (Route<dynamic> route) => false);
  }

  void updateData(String id, String status) async
  {

    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    var data = await networkHelper.update(prefs.getString("token")!, status, id);
  }

  void check() async
  {

    //log("check");

    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');

    var data = await networkHelper.check(token!!);
    var data1 = json.decode(data.body);
    Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';
    String tokens = map['token'];

    //log("sajib --- "+tokens);
    if(token=='admin')
    {
      //log(token);

      if(token.toString()=="admin")
      {
        //log(tokens!!);
        //Navigator.pushNamedAndRemoveUntil(context, "/admin_home", (Route<dynamic> route) => false);
      }
      else if(token.toString().length>10)
      {
        //log(tokens!!);
        Navigator.pushNamedAndRemoveUntil(context, "/sub_home", (Route<dynamic> route) => false);
      }
    }
    else
    {
      prefs.remove('token');
      Navigator.pushNamedAndRemoveUntil(context, "/", (Route<dynamic> route) => false);
    }



  }

  @override
  void initState() {
    // TODO: implement initState
    log("init =======");
    setState(() {
      check();
      testData();
      build(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    setState(() {
      testData();
      check();
    });

    return Scaffold(
      backgroundColor: Colors.purple[100],
      body: Column(
        children: <Widget>[

          const SizedBox(height: 20, width: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 20, width: 20),
              // DropdownButton<String>(
              //   hint: Text(status),
              //   items: <String>['Taken', 'Pending', 'Closed'].map((String value) {
              //     return DropdownMenuItem<String>(
              //       value: value,
              //       child: Text(value, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
              //     );
              //   }).toList(),
              //   onChanged: (values) {
              //     status = values!;
              //     log("ssss    "+status);
              //     testData();
              //     check();
              //   },
              // ),

              Container(
                child: RawMaterialButton(
                  fillColor: Colors.purple[300],
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
                  padding: const EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, "/admin_users", (Route<dynamic> route) => false);
                  },
                  child: const Text("Users", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                ),
              ),

              Container(width: 20,),
              DropdownButton(
                // Initial Value
                value: status,
                // Down Arrow Icon
                icon: const Icon(Icons.keyboard_arrow_down),
                // Array list of items
                items: item.map((String items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items),
                  );
                }).toList(),
                // After selecting the desired option,it will
                // change button value to selected value
                onChanged: (String? newValue) {
                  //log("testing ----");
                  setState(() {
                    status = newValue!;
                    testData();
                    check();
                    build(context);
                  });
                  testData();
                },
              ),
              Container(
                child: RawMaterialButton(
                  fillColor: Colors.black,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
                  padding: const EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
                  onPressed: () {
                    logout();
                  },
                  child: const Text("Sign out", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20, width: 20),

          Expanded(
            child: Container(
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: data_s,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: 50,
                        //child: Center(child: Text('${items[index]['name']}')),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white
                        ),
                        child: Column(
                          children: [
                            SizedBox( height:10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('${items[index]['name']}'),
                                Text('${items[index]['phoneNumber']}'),
                                Text('${items[index]['branch']}'),
                                Text('${items[index]['message']}'),
                              ],
                            ),
                            SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('${items[index]['requestedDoctor']}'),
                                Text('${items[index]['requestedTime']} - ${items[index]['requestedDate']}'),
                                //Text('${items[index]['status']}'),
                                Container(
                                  child: RawMaterialButton(
                                      child: Text('${items[index]['status']}'),
                                      onPressed: () {

                                        setState(() {
                                          id = items[index]['id'];
                                        });

                                        Dialog dialog = Dialog(
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                                          child: Container(
                                            height: 150.0,
                                            width: 200.0,

                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[

                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Taken'),
                                                      onPressed: () {
                                                        updateData(id.toString(), "Taken");
                                                        setState(() {
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context).pop();
                                                      }
                                                  ),
                                                ),

                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Pending'),
                                                      onPressed: () {
                                                        log("here pending "+id.toString()+"  ");
                                                        setState(() {
                                                          updateData(id.toString(), "Pending");
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context).pop();
                                                      }
                                                  ),
                                                ),

                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Closed'),
                                                      onPressed: () {
                                                        updateData(id.toString(), "Closed");
                                                        setState(() {
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context).pop();
                                                      }
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );

                                        showDialog(context: context, builder: (BuildContext context) => dialog);
                                      }
                                  ),
                                ),
                                Text('${items[index]['emailAddress']}')
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  }
              ),
            ),
          )
        ],
      ),
    );


  }
}
