import 'dart:convert';
import 'dart:html';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkHelper {

  var url = 'http://localhost:8080';

  Future loginData(String phone, String password) async
  {
    return post(
      Uri.parse(url+'/home/login'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'phone': phone,
        'password': password
      }),
    );
  }

  Future check(String token) async
  {
    return post(
      Uri.parse(url+'/home/check'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token,
      }),
    );
  }

  Future getAppointment(String token, String status) async
  {
    return post(
      Uri.parse(url+'/home/get/'+status),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }

  Future getUsers(String token) async
  {
    return post(
      Uri.parse(url+'/home/getUsers'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }

  Future update(String token, String status, String id) async
  {
    return post(
      Uri.parse(url+'/home/update/'+id+"/"+status),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }

  Future updateUser(String token, String branch, String id) async
  {
    return post(
      Uri.parse(url+'/home/updateUser/'+id+"/"+branch),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }


  Future saveUser(String token, String name, String password, String phone, String branch) async
  {
    return post(
      Uri.parse(url+'/home/saveUser/'+name+"/"+password+"/"+phone+"/"+branch),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }

  Future deleteUser(String token, int id) async
  {
    return post(
      Uri.parse(url+'/home/deleteUser/'+id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'token': token
      }),
    );
  }

  Future getAnimal(String animal) async
  {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? token = prefs.getString('token');


    return post(
      Uri.parse(url+'/haat/animals/$animal'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'name': token!!
      }),
    );
  }

  Future getUser() async
  {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? token = prefs.getString('token');

    return post(
      Uri.parse(url+'/haat/user'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'name': token.toString()
      }),
    );
  }

}