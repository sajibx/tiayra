import 'package:flutter/material.dart';
import 'package:tiayra_admin/responsive/desktop_body.dart';
import 'package:tiayra_admin/responsive/mobile_body.dart';
import 'package:tiayra_admin/responsive/responsive_layout.dart';
import 'package:tiayra_admin/responsive/tablet_body.dart';
import 'package:tiayra_admin/ui/admin_users.dart';
import 'package:tiayra_admin/ui/home.dart';
import 'package:tiayra_admin/ui/home_desk.dart';
import 'package:tiayra_admin/util/admin_home.dart';
import 'package:tiayra_admin/util/sub_home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // @override
  // Widget build(BuildContext context) {
  //   return MaterialApp(
  //     debugShowCheckedModeBanner: false,
  //     theme: ThemeData(primarySwatch: Colors.green),
  //     home: ResponsiveLayout(
  //       mobileBody: const MobileScaffold(),
  //       tabletBody: const TabletScaffold(),
  //       desktopBody: const home_desk(),
  //     ),
  //   );
  // }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => ResponsiveLayout(
              mobileBody: const MobileScaffold(),
              tabletBody: const home_desk(),
              desktopBody: const home_desk(),
            ),
        '/admin_home': (context) => ResponsiveLayout(
              mobileBody: const MobileScaffold(),
              tabletBody: const admin_home(),
              desktopBody: const admin_home(),
            ),
        '/sub_home': (context) => ResponsiveLayout(
              mobileBody: const MobileScaffold(),
              tabletBody: const sub_home(),
              desktopBody: const sub_home(),
            ),
        '/admin_users': (context) => ResponsiveLayout(
              mobileBody: const MobileScaffold(),
              tabletBody: const admin_users(),
              desktopBody: const admin_users(),
            ),
      },
    );
  }
}
