import 'dart:convert';
import 'dart:developer';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/NetworkHelper.dart';

class admin_users extends StatefulWidget {
  const admin_users({Key? key}) : super(key: key);

  @override
  State<admin_users> createState() => _admin_usersState();
}

class _admin_usersState extends State<admin_users> {
  int data_s = 0;
  var items;
  int id = 0;

  var item = ['Bashudhara', 'Eskaton', 'Uttora'];

  String name = "";
  String password = "";
  String phone = "";
  String branch = "Bashudhara";

  void logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Navigator.pushNamedAndRemoveUntil(
        context, "/", (Route<dynamic> route) => false);
  }

  void check() async {
    //log("check");

    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');

    var data = await networkHelper.check(token!!);
    var data1 = json.decode(data.body);
    Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';
    String tokens = map['token'];

    //log("sajib --- "+tokens);
    if (token == 'admin') {
      //log(token);

      if (token.toString() == "admin") {
        //log(tokens!!);
        //Navigator.pushNamedAndRemoveUntil(context, "/admin_home", (Route<dynamic> route) => false);
      } else if (token.toString().length > 10) {
        //log(tokens!!);
        Navigator.pushNamedAndRemoveUntil(
            context, "/sub_home", (Route<dynamic> route) => false);
      }
    } else {
      prefs.remove('token');
      Navigator.pushNamedAndRemoveUntil(
          context, "/", (Route<dynamic> route) => false);
    }
  }

  void testData() async {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    //log("here 1");
    var data = await networkHelper.getUsers(prefs.getString('token')!);
    var data_ = json.decode(data.body);

    //log("sajib 2 - ");

    //Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';
    var map = jsonDecode(data.body); // import 'dart:convert';
    setState(() {
      data_s = data_.length;
      items = map;
    });

    // log("sajib 1 - "+map[0]['name']);
    // //final datas = data_ as Map;
    // log("sajib 1 - "+data_.length.toString());
    // log("sajib 1 - "+data_s.toString()+"----"+map[0]['name']);
  }

  void updateData(String id, String status) async {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    log("sajib " + id + "  " + status);
    var data = await networkHelper.updateUser(prefs.getString("token")!, status, id);
  }

  void saveUser() async {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if(name.length>0&&password.length>0&&branch.length>0)
      {
        var data = await networkHelper.saveUser(prefs.getString("token")!, name, password, phone, branch);
      }
  }

  void deleteUser(int id) async {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = await networkHelper.deleteUser(prefs.getString("token")!, id);
  }

  @override
  void initState() {
    // TODO: implement initState
    log("init =======");
    setState(() {
      check();
      testData();
      build(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      testData();
      check();
    });

    return Scaffold(
      backgroundColor: Colors.purple[100],
      body: Column(
        children: [
          SizedBox(height: 20, width: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 20, width: 20),
              Container(
                child: RawMaterialButton(
                  fillColor: Colors.purple[300],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40)),
                  padding: const EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, "/admin_home",
                        (Route<dynamic> route) => false);
                  },
                  child: const Text(
                    "Appointments",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                child: RawMaterialButton(
                  fillColor: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40)),
                  padding: const EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
                  onPressed: () {
                    //logout();

                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text(''),
                            content: Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                TextField(
                                  onChanged: (value) {
                                    name = value;
                                  },
                                  //controller: _textFieldController,
                                  decoration: InputDecoration(hintText: "Name"),
                                ),
                                TextField(
                                  onChanged: (value) {
                                    password = value;
                                  },
                                  //controller: _textFieldController,
                                  decoration:
                                      InputDecoration(hintText: "Password"),
                                ),
                                TextField(
                                  onChanged: (value) {
                                    phone = value;
                                  },
                                  //controller: _textFieldController,
                                  decoration:
                                      InputDecoration(hintText: "Phone"),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DropdownButton(
                                  // Initial Value
                                  value: branch,
                                  // Down Arrow Icon
                                  icon: const Icon(Icons.keyboard_arrow_down),
                                  // Array list of items
                                  items: item.map((String items) {
                                    return DropdownMenuItem(
                                      value: items,
                                      child: Text(items),
                                    );
                                  }).toList(),
                                  // After selecting the desired option,it will
                                  // change button value to selected value
                                  onChanged: (String? newValue) {
                                    //log("testing ----");
                                    setState(() {
                                      branch = newValue!;
                                      testData();
                                      check();
                                      build(context);
                                    });
                                    testData();
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    fillColor: Colors.purple[300],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(40)),
                                    padding: const EdgeInsets.fromLTRB(
                                        60.0, 20.0, 60.0, 20.0),
                                    onPressed: () {
                                      saveUser();
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      "Add User",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        });

                  },
                  child: const Text(
                    "Add User",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                child: RawMaterialButton(
                  fillColor: Colors.black,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40)),
                  padding: const EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
                  onPressed: () {
                    logout();
                  },
                  child: const Text(
                    "Sign out",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const SizedBox(height: 20, width: 20),
            ],
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: data_s,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: 50,
                        //child: Center(child: Text('${items[index]['name']}')),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('${items[index]['username']}'),
                                Text('${items[index]['password']}'),
                                //Text('${items[index]['branch']}'),

                                Container(
                                  child: RawMaterialButton(
                                      child: Text('${items[index]['branch']}'),
                                      onPressed: () {
                                        setState(() {
                                          id = items[index]['id'];
                                        });

                                        Dialog dialog = Dialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(12.0)),
                                          //this right here
                                          child: Container(
                                            height: 150.0,
                                            width: 200.0,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Bashudhara'),
                                                      onPressed: () {
                                                        updateData(
                                                            id.toString(),
                                                            "Bashudhara");
                                                        setState(() {
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context)
                                                            .pop();
                                                      }),
                                                ),
                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Eskaton'),
                                                      onPressed: () {
                                                        log("here pending " +
                                                            id.toString() +
                                                            "  ");
                                                        setState(() {
                                                          updateData(
                                                              id.toString(),
                                                              "Eskaton");
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context)
                                                            .pop();
                                                      }),
                                                ),
                                                Container(
                                                  child: RawMaterialButton(
                                                      child: Text('Uttora'),
                                                      onPressed: () {
                                                        updateData(
                                                            id.toString(),
                                                            "Uttora");
                                                        setState(() {
                                                          testData();
                                                          check();
                                                        });
                                                        Navigator.of(context)
                                                            .pop();
                                                      }),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );

                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) =>
                                                dialog);
                                      }),
                                ),

                                Text('${items[index]['phone']}'),

                                Container(
                                  child: RawMaterialButton(
                                    fillColor: Colors.red,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(40)),
                                    padding: const EdgeInsets.fromLTRB(
                                        2.0, 2.0, 2.0, 2.0),
                                    onPressed: () {
                                      deleteUser(items[index]['id']);
                                      setState(() {
                                        testData();
                                        check();
                                      });
                                    },
                                    child: const Text(
                                      "Delete",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            //   children: [
                            //     Text('${items[index]['requestedDoctor']}'),
                            //     Text('${items[index]['requestedTime']} - ${items[index]['requestedDate']}'),
                            //     //Text('${items[index]['status']}'),
                            //     // Container(
                            //     //   child: RawMaterialButton(
                            //     //       child: Text('${items[index]['status']}'),
                            //     //       onPressed: () {
                            //     //
                            //     //         setState(() {
                            //     //           id = items[index]['id'];
                            //     //         });
                            //     //
                            //     //         Dialog dialog = Dialog(
                            //     //           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                            //     //           child: Container(
                            //     //             height: 150.0,
                            //     //             width: 200.0,
                            //     //
                            //     //             child: Column(
                            //     //               mainAxisAlignment: MainAxisAlignment.center,
                            //     //               children: <Widget>[
                            //     //
                            //     //                 Container(
                            //     //                   child: RawMaterialButton(
                            //     //                       child: Text('Taken'),
                            //     //                       onPressed: () {
                            //     //                         updateData(id.toString(), "Taken");
                            //     //                         setState(() {
                            //     //                           testData();
                            //     //                           check();
                            //     //                         });
                            //     //                         Navigator.of(context).pop();
                            //     //                       }
                            //     //                   ),
                            //     //                 ),
                            //     //
                            //     //                 Container(
                            //     //                   child: RawMaterialButton(
                            //     //                       child: Text('Pending'),
                            //     //                       onPressed: () {
                            //     //                         log("here pending "+id.toString()+"  ");
                            //     //                         setState(() {
                            //     //                           updateData(id.toString(), "Pending");
                            //     //                           testData();
                            //     //                           check();
                            //     //                         });
                            //     //                         Navigator.of(context).pop();
                            //     //                       }
                            //     //                   ),
                            //     //                 ),
                            //     //
                            //     //                 Container(
                            //     //                   child: RawMaterialButton(
                            //     //                       child: Text('Closed'),
                            //     //                       onPressed: () {
                            //     //                         updateData(id.toString(), "Closed");
                            //     //                         setState(() {
                            //     //                           testData();
                            //     //                           check();
                            //     //                         });
                            //     //                         Navigator.of(context).pop();
                            //     //                       }
                            //     //                   ),
                            //     //                 ),
                            //     //               ],
                            //     //             ),
                            //     //           ),
                            //     //         );
                            //     //
                            //     //         showDialog(context: context, builder: (BuildContext context) => dialog);
                            //     //       }
                            //     //   ),
                            //     // ),
                            //     Text('${items[index]['emailAddress']}')
                            //   ],
                            // ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          )
        ],
      ),
    );
  }
}
