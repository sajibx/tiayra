import 'package:flutter/material.dart';

class home extends StatelessWidget {
  const home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            const SizedBox(height: 20,),
            Center(
              child: Container(
                child: const Text(
                  'Tiayra',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20
                  ),
                  textScaleFactor: 2.0,
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 50,
              width: 350,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.grey[200]),
            ),
            const SizedBox(height: 10,),
            Container(
              height: 50,
              width: 350,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.grey[200]),
              child: const Center(
                child: TextField(
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 10
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Container(
              height: 50,
              width: 150,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.green[200]),
              child: const Center(
                child: Text(
                  'Login',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 10
                  ),
                  textScaleFactor: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
