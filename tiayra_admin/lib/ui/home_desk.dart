import 'dart:convert';
import 'dart:developer';

import 'package:alert/alert.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/NetworkHelper.dart';

class home_desk extends StatefulWidget {
  const home_desk({Key? key}) : super(key: key);

  @override
  State<home_desk> createState() => _home_deskState();
}

class _home_deskState extends State<home_desk> {

  late String phone;
  late String password;


  void testData(String phone, String password) async
  {
    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // final String? token = prefs.getString('token');
    //log("here");
    var data = await networkHelper.loginData(phone, password);
    var data1 = json.decode(data.body);
    //log(data.body);

    Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';

    String token = map['token'];


    if(token.toString()=="admin")
    {
      log("sajib if");
      prefs.setString('token', token);
      var test = prefs.getString('token');
      //log(test!!);
      Navigator.pushNamedAndRemoveUntil(context, "/admin_home", (Route<dynamic> route) => false);
    }
    else if(data1.toString().length>5)
    {
      log("sajib else if");
      prefs.setString('token', token);
      var test = prefs.getString('token');
      //log(test!!);
      Navigator.pushNamedAndRemoveUntil(context, "/sub_home", (Route<dynamic> route) => false);
    }
    else
    {
      log("sajib else");
      //Alert(message: token.toString()).show();
    }
  }

  void check() async
  {

    log("check");

    NetworkHelper networkHelper = NetworkHelper();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');

    var data = await networkHelper.check(token!!);
    var data1 = json.decode(data.body);
    Map<String, dynamic> map = jsonDecode(data.body); // import 'dart:convert';
    String tokens = map['token'];

    log(token);

    if(tokens.toString()=="admin")
    {
      //log(tokens!!);
      Navigator.pushNamedAndRemoveUntil(context, "/admin_home", (Route<dynamic> route) => false);
    }
    else if(tokens.toString().length>6)
    {
      //log(tokens!!);
      Navigator.pushNamedAndRemoveUntil(context, "/sub_home", (Route<dynamic> route) => false);
    }
    else
    {
      prefs.remove('token');
      Navigator.pushNamedAndRemoveUntil(context, "/", (Route<dynamic> route) => false);
    }

  }

  @override
  Widget build(BuildContext context) {
    //check();
    return Scaffold(
      backgroundColor: Colors.purple[100],
      body: Container(
        child: Column(
          children: [
            const SizedBox(height: 20,),
            Center(
              child: Container(
                child: const Text(
                  'Tiayra',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20
                  ),
                  textScaleFactor: 2.0,
                ),
              ),
            ),
            const SizedBox(height: 20,),


            Container(
              width: 350,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(35.0, 10.0, 35.0, 10.0),
                child: TextField(
                  onChanged: (value) {
                    phone = value;
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Phone",
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width:  0.5),
                        borderRadius: BorderRadius.circular(20.0)
                    ),
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.blueGrey, width: 0.5),
                        borderRadius: BorderRadius.circular(20.0)
                    ),
                  ),
                ),
              ),
            ),

            Container(
              width: 350,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(35.0, 10.0, 35.0, 10.0),
                child: TextField(
                  onChanged: (value) {
                    password = value;
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Password",
                    focusedBorder:OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.blueGrey, width: 0.5),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.blueGrey, width: 0.5),
                        borderRadius: BorderRadius.circular(20.0)
                    ),
                  ),
                ),
              ),
            ),

            SizedBox(height: 10.0,),

            RawMaterialButton(
              fillColor: Colors.black,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40)),
              child: Text("Sign in", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
              padding: EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
              onPressed: () {
                testData(phone, password);
              },
            ),

          ],
        ),
      ),
    );
  }
}
